<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'codelineW');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'V?F^ALjU!T[3>-f?aB*gqLXB0oTZAapGFzDBEga7n!O.Gnl{>i[RS#D.m^>qkZ~9');
define('SECURE_AUTH_KEY',  'r4OQaH%9yIzhQ_-<{>#IbWb?3slC?Y5!J.N8yIFgK2YcM@HA}+lb#t]l;pFu rOm');
define('LOGGED_IN_KEY',    '`D=@s%&/T)]Lk%ONY|[))P^ThucJU.*X^_Hs=>7GNSrQ l6hsFbpo%MjQeto(V~)');
define('NONCE_KEY',        ',BK.i:c)9EQ-E]?sjY!!dB36iJXj3&Nu~YGr?|JrQEr}FR1~f@::lYOKk5/oStV ');
define('AUTH_SALT',        '|#jTtxl0OQF v.+`b(v{+aNg%R-ZBF94w}-R5$e;hVaPPVt;,Y8Jp^B&@m(R+*>8');
define('SECURE_AUTH_SALT', 'K#,[i)+/w2 T2y+0qGP<^R~D#nGS}te?(N[{N_gQ7~?=c2~(E#40L@goZjr?C_!#');
define('LOGGED_IN_SALT',   '1OZA%`buN4qZ[=;;1:AE=QbE_!o]8!?)&Deu[?6cIN_vk80(N&}qv{E F+VMMV=H');
define('NONCE_SALT',       'E~myfa^F!#nw2~Uz8x.n>!7.d`UT/.jH*L44=VSm $&P|&NE(kveu]~=cgAzutn?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
